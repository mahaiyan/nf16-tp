```c
int rechercherValeur(matrice_creuse m, int i, int j){
    while(i < 1 || i > m.Nlignes){
        printf("Numero de la ligne incorrect, entrez de nouveau : ");
        scanf("%d", &i);
    }
    while(j < 1 || j > m.Ncolonnes){
        printf("Numero de la colonne incorrect, entrez de nouveau : ");
        scanf("%d", &j);
    }
    i--;
    j--;
    element* ptr = *(m.tableau + i);
    while(ptr && ptr->col < j){
        ptr = ptr->suivant;
    }
    if(!ptr) // chaîne terminée avant avoir trouvé le noeud
        return 0;
    if(ptr->col == j) // noeud trouvé
        return ptr->val;
    // ptr->col > j
    // le noeud n'existe pas, le valeur est donc 0
    return 0;
}
```

Première partie avec les while :
Pour i : complexité de O(n)
Pour j : complexité de O(m)
N et m étant le nombre d’entrée utilisateur fausses.
Deuxième partie :
~~La complexité de la boucle while pour le i est de O(1). En effet, le i est la ligne d’un tableau des listes chainées. Donc, comme dans la fonction rechercherValeur on donne directement le i a trouver et qu’il se trouve dans un tableau, on a O(1). ~~On utilise le i transmet pour obtenir directement la tête de la ligne i, donc on a O(1).
Pour le j, c’est différent car c’est la colonne d’une liste chainée. Ainsi, on va être obligé de la parcourir pour arriver au j. Le pire des cas possible étant de ne pas trouver j après avoir tout parcouru : donc la complexité est de O(n), n étant ici la taille de la liste chainée parcourue.

```c
int getPositif(){
    int n;
    do{
        printf("L'entree doit être strictement positif : ");
        scanf("%d", &n);
    }while(n < 1);
    return n;
}
```

Le pire des cas est de ne faire que des chiffres inférieurs à 1 en boucle : donc n fois.
La complexité est : O(n)

```c
int colonneElement(){
    printf("Quelle est le numero de colonne de l'element ? ");
    return getPositif();
}
```

Ici la complexité de printf est de O(1).
Mais ensuite on fait appel à la fonction getPositif : qui a une complexité de O(n).
Donc la complexité de colonneElement est : O(n) (car entre O(n) et O(1), c’est celle qui a l’ordre le plus grand).

```c
int nombreMatrice(){
    printf("Quel est le nombre total des matrices ? ");
    return getPositif();
}
int nombreLigne(){
    printf("Quelle est le nombre de ligne de la matrice ? ");
    return getPositif();
}

int nombreColonne(){
    printf("Quelle est le nombre de colonne de la matrice ? ");
    return getPositif();
}

int ligneElement(){
    printf("Quelle est le numero de ligne de l'element ? ");
    return getPositif();
}
```

Les fonctions ligneElement, nombreMatrice, nombreColonne se construisent de la même manière que colonneElement : le printf est de O(1) et l’appel à getPositif est de O(n).
Donc la complexité de ces fonctions est de O(n), n étant ici le nombre d’entrées fausses de l’opérateur.

```c
void supprimerMatrice(matrice_creuse *m){
    int i, j;
    element** ptr;
    element* tmp;
    for(i=0; i<m->Nlignes; i++){
        ptr = m->tableau + i;
        while(*ptr){
            tmp = *ptr;
            ptr = &(tmp->suivant);
            free(tmp);
        }
    }
    free(m->tableau);
    printf("Matrice supprimee.\n");
}
```

La boucle for va de 0 au nombre de lignes.
~~La boucle while a une complexité de O(1). ~~
La boucle while a une complexité de O(m), m étant le nombre de noeuds dans une ligne.
Donc la complexité de supprimerMatrice est de O(n\*m), n étant ici le nombre de lignes.

```c
void malloc_erreur(void* p){
    if(!p)
        perror("Espace insuffisant. ");
}
```

La complexité de la fonction malloc_erreur est de O(1).

```c
matrice_creuse* init(int nMatrice){
    matrice_creuse *m = malloc(nMatrice * sizeof(matrice_creuse)); //permet de donner de l’espace mémoire à la matrice
    malloc_erreur(m); // vérifier si on a bien obtenu une allocation
    printf("\n"
        "\t ____________________________________________________________\n"
        "\t|1. Remplir une matrice creuse                               |\n"
        "\t|2. Afficher une matrice creuse sous forme de tableau        |\n"
        "\t|3. Afficher une matrice creuse sous forme de listes         |\n"
        "\t|4. Donner la valeur d’un element d’une matrice creuse       |\n"
        "\t|5. Affecter une valeur a un element d’une matrice creuse    |\n"
        "\t|6. Additionner deux matrices creuses                        |\n"
        "\t|7. Calculer le gain en espace                               |\n"
        "\t|8. Quitter                                                  |\n"
        "\t|____________________________________________________________|\n"
        "*** Attention : les numeros (de matrice, de ligne, de colonne) commencent par 1 au lieu de 0 ***\n\n");
    return m;
}
```

~~La complexité de la première itération est de O(1).
La complexité de la seconde itération est O(1) car la fonction malloc_erreur est de O(1).
Puis le reste est aussi de O(1).~~Il n'y a aucun boucle dans la fonction init et la fonction malloc_erreur.
Donc la complexité de init est de O(1).

```c
int choixManip(){
    int choix;
    do{
        printf("\nQuelle est votre manipulation (entre 1 et 8) ? ");
        scanf("%d", &choix);
    }while(choix < 1 || choix > 8);
    return choix;
}
```

~~La boucle do while recommence pour choix entre – l’infini et 1 et 8 et + l’infini. Donc si choix tend vers n n’appartenant pas à [1 ;8].~~
Donc la complexité de la fonction choixManip est de O(n), n étant le nombre d'entrées invalides.

```c
int valElement(){
    printf("Quelle est la valeur de l'element ? ");
    int val;
    scanf("%d", &val); // *** ne pas utiliser getPositif !
    return val;
}
```

La complexité de la fonction valElement est de O(1) car ici on a O(4)=O(1).

```c
int numeroMatrice(int max){
    printf("Quelle est le numero de la matrice a manipuler ? ");
    int n;
    do{
        printf("L'entree doit ne pas depasser le nombre total des matrices, %d. ", max);
        n = getPositif();
    }while(n > max);
    return n;
}
```

La première itération est de O(1), pareil pour la deuxième.
Pour la boucle do while, le pire des cas est que l’on écrive n fois un n supérieur à max. Dans le do, on fait appel à la fonction getPositif qui est de complexité O(m).
Donc la complexité de numeroMatrice est de O(n\*m).
O(n) (si on concidère que n est l’entrée utilisateur et qu’il y a seulement une condition supp avec cette nouvelle fonction mais que c’est le même n entré. ?????????

```c
void remplirMatrice(matrice_creuse *m, int N, int M){
    m->tableau = malloc(N * sizeof(liste_ligne));
    malloc_erreur(m->tableau); // vérifier si on a bien obtenu une allocation
    m->Nlignes = N;
    m->Ncolonnes = M;
    printf("Veuillez entrer une matrice de la taille %d * %d :\n", N, M);
    int val;
    for(int i=0; i<N; i++){
        element** ptr = m->tableau + i;
        *ptr = 0;
        for(int j=0; j<M; j++){
            scanf("%d", &val);
            if(!val)
                continue;
            // val != 0
            element* tmp = malloc(sizeof(element));
            malloc_erreur(tmp); // vérifier si on a bien obtenu une allocation
            tmp->col = j;
            tmp->val = val;
            tmp->suivant = 0;
            *ptr = tmp;
            ptr = &(tmp->suivant);
        }
    }
}
```

~~1ère itération : O(1)
2ème itération : O(1)
Etc O(1) jusqu’à la 6eme  itération.~~
Boucle for : on va de 0 à N. N étant le nombre de lignes au total.
Deuxième boucle for : on va de 0 à M, M étant le nombre de colonnes au total.
Le pire des cas possible est d'avoir des valeurs non nulles pour chaque colonne, et devoir donc créer un noeud pour chaque colonne.
Ainsi, la complexité de la fonction remplirMatrice est de O(N\*M).

```c
void afficherMatrice(matrice_creuse m){
    int i,j;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        for(j=0; j<m.Ncolonnes; j++){
            if(!ptr){ // traiter la fin de la chaîne
                for(; j<m.Ncolonnes; j++)
                    print0;
                break;
            }
            if(ptr->col > j){ // remplir les 0
                for(; j<ptr->col; j++)
                    print0;
            }
            // ptr->col == j
            printval(ptr->val);
            ptr = ptr->suivant;
        }
        printf("\n");
    }
}
```

La boucle for va de 0 à m.Nlignes, c’est-à-dire le numéro de lignes de la matrice m.
La deuxième boucle for va de 0 à m.Ncolonnes, c’est-à-dire le numéro de colonnes de la matrice m.
~~Ici, le pire des cas est de continuer de remplir les 0. Or, pour remplir le maximum de zéro, il faut remplir la taille totale maximale de la matrice m. Sa taille maximale est de m.Nlignes*m.Ncolonnes : soit i*j~~
Donc la complexité sera O(N\*M) avec N le nombre de lignes totale de la matrice m et M le nombre total de colonnes de la matrice m.

```c
void afficherMatriceListes(matrice_creuse m){
    int i;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        while(ptr){
            printinfo(ptr->col, ptr->val);
            ptr = ptr->suivant;
        }
        // la fin de la chaîne
        printfin;
    }
}
```

La boucle for va de 0 à m.Nlignes, c’est-à-dire le numéro de lignes de la matrice m.
Ici, le pire des cas est d’aller au bout (d’afficher tous les chiffres qu’il est possible d’avoir saisi) de la liste chainée car il n’y a pas de 0 (on ne saisit pas de zéro dans la liste chainée). Or, le bout de la liste chainée est au maximum de taille Ncolonne (nombre total de colonne de la matrice).
Donc la complexité sera O(N\*M) avec N le nombre de lignes totale de la matrice m et M le nombre total de colonnes de la matrice m.

```c
void affecterValeur(matrice_creuse m, int i, int j, int val){
    // verifier la validite de i et j dans la fonction rechercherValeur
    int val0 = rechercherValeur(m, i, j); // matrice creuse donc val0 == 0 la plus part du temp
    // l'ordre est spécifié pour qu'il soit moins coûteux que possible
    i--;
    j--;
    if(val == val0){ // rien à modifier
        printf("valeur identique.\n");
        return;
    }
    if(val == 0){ // val0 != 0, supprimer un noeud
        element** ptr = m.tableau + i;
        element* tmp;
        while((*ptr)->col < j) // val0 non 0 donc noeud existe toujours
            ptr = &(*ptr)->suivant;
        // (*ptr)->col == j
        tmp = *ptr;
        *ptr = tmp->suivant;
        free(tmp);
        printf("noeud supprime.\n");
        return;
    }
    if(val0 == 0){ // val != 0, rajouter un noeud
        element** ptr = m.tableau + i;
        while ((*ptr) && (*ptr)->col < j) // chercher
            ptr = &((*ptr)->suivant);
        // (*ptr)->col > j ou (*ptr)==0, donc trouvé
        element *tmp = malloc(sizeof(element)); // *** déboggué ***
        tmp->col = j;
        tmp->val = val;
        tmp->suivant = *ptr;
        *ptr = tmp;
        printf("noeud rajoute.\n");
        return;
    }
    // noeud existe déjà, changer la valeur
    element* ptr = *(m.tableau + i);
    while(ptr->col < j){
        ptr = ptr->suivant;
    }
    ptr->val = val;
    printf("valeur changee.\n");
    return;
}

void additionnerMatrices(matrice_creuse m1, matrice_creuse m2){
    element **p1, **p2, *t;
    for(int i=0; i<m1.Nlignes; i++){
        p1 = m1.tableau + i;
        p2 = m2.tableau + i;
        while (*p2) // on traite m2, pour modifier m1
        {
            while (*p1 && (*p1)->col < (*p2)->col)
                p1 = &((*p1)->suivant);
            if(!*p1){ // m1 terminé, copier tous les noeud de m2
                while(*p2){
                    t = malloc(sizeof(element)); // *** utlisier toujours malloc pour ajouter un noeud ***
                    t->col = (*p2)->col;
                    t->val = (*p2)->val;
                    t->suivant = 0;
                    *p1 = t;
                    p1 = &(t->suivant);
                }
                break; // terminer la ligne
            }
            if((*p1)->col > (*p2)->col){ // noeud repéré, copier un seul noeud
                t = malloc(sizeof(element));
                t->col = (*p2)->col;
                t->val = (*p2)->val;
                t->suivant = *p1;
                *p1 = t;
                p1 = &(t->suivant);
                p2 = &((*p2)->suivant); // *** noeud traité, avancer ***
                continue;
            }
            // (*p1)->col == (*p2)->col, ajouter les valeurs
            (*p1)->val += (*p2)->val;
            if(!(*p1)->val){
                t = (*p1)->suivant;
                free(*p1);
                *p1 = t;
            }
            p2 = &((*p2)->suivant); // noeud traité, avancer
        }
    }
}

int nombreOctetsGagnes(matrice_creuse m){
    int i,s=0;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        while(ptr){
            ptr = ptr->suivant;
            s++; // compter le nombre des noeuds
        }
    }
    s *= sizeof(element); // octets des noeuds, incluant l'information de colonne et pointeur
    s += m.Nlignes * sizeof(liste_ligne); // octets de liste_ligne
    s += sizeof(matrice_creuse); // octets de matrice
    return m.Ncolonnes * m.Nlignes * sizeof(int) - s;
}
```
