# Rapport de NF16 TP3

## Auteurs : Haiyang MA, Lola TRUPIN

### Structures principales

```c
typedef struct element{
    int col;
    int val;
    struct element* suivant;
}element;

typedef element* liste_ligne;

typedef struct matrice_creuse{
    liste_ligne* tableau; // pointeur de pointeur
    int Nlignes;
    int Ncolonnes;
}matrice_creuse;
```

### Fonctions supplémentaires

* void malloc_erreur(void* p)

```c
void malloc_erreur(void* p){
    if(!p)
        perror("espace insuffisant, erreur °%d ");
}
```

Malloc_erreur va permettre d'afficher un message d'erreur pour cause d'espace insufisant.

* macro

```
#define print0 printf("0\t")
#define printfin printf("fin\n")
#define printval(X) printf("%d\t", X)
#define printinfo(COL,VAL) printf("col:%d val:%d\t",COL,VAL)
```

On utilise les macros pour une structure qui a plus de sens et qui se modifie plus facilement

* int getPositif()

```c
int getPositif(){
    int n;
    do{
        printf("L'entree doit être strictement positif : ");
        scanf("%d", &n);
    }while(n < 1);
    return n;
}
```

getPositif permet de demander à l'utilisateur de rentrer une valeur positive.
Le pire des cas est de ne faire que des chiffres inférieurs à 1 en boucle : donc n fois.
La complexité est : O(n)

```c
int colonneElement(){
    printf("Quelle est le numero de colonne de l'element ? ");
    return getPositif();
}
```

Cela permet à l'utilisateur de saisir le numéro de colonne de l'élément.
Ici la complexité de printf est de O(1).
Mais ensuite on fait appel à la fonction getPositif : qui a une complexité de O(n).
Donc la complexité de colonneElement est : O(n) (car entre O(n) et O(1), c’est celle qui a l’ordre le plus grand).

```c
int nombreMatrice(){
    printf("Quel est le nombre total des matrices ? ");
    return getPositif();
}
int nombreLigne(){
    printf("Quelle est le nombre de ligne de la matrice ? ");
    return getPositif();
}

int nombreColonne(){
    printf("Quelle est le nombre de colonne de la matrice ? ");
    return getPositif();
}

int ligneElement(){
    printf("Quelle est le numero de ligne de l'element ? ");
    return getPositif();
}
```

Cela permet de saisir :

1. le nombre total des matrices
2. le nombre de ligne de la matrice
3. le nombre de colonne de la matrice
4. le numero de ligne de l'element

Les fonctions ligneElement, nombreMatrice, nombreColonne se construisent de la même manière que colonneElement : le printf est de O(1) et l’appel à getPositif est de O(n).
Donc la complexité de ces fonctions est de O(n), n étant ici le nombre d’entrées fausses de l’opérateur.

* void supprimerMatrice(matrice_creuse \*m)

```c
void supprimerMatrice(matrice_creuse *m){
    int i, j;
    element** ptr;
    element* tmp;
    for(i=0; i<m->Nlignes; i++){
        ptr = m->tableau + i;
        while(*ptr){
            tmp = *ptr;
            ptr = &(tmp->suivant);
            free(tmp);
        }
    }
    free(m->tableau);
    printf("Matrice supprimee.\n");
}
```

SupprimerMatrice permet d'effacer une matrice.
La boucle for va de 0 au nombre de lignes.
La boucle while a une complexité de O(1).
La boucle while a une complexité de O(m), m étant le nombre de noeuds dans une ligne.
Donc la complexité de supprimerMatrice est de O(n\*m), n étant ici le nombre de lignes.

* void malloc_erreur(void* p)

```c
void malloc_erreur(void* p){
    if(!p)
        perror("Espace insuffisant. ");
}
```

Cela permet d'indiquer s'il n'y a pas assez d'espace.
La complexité de la fonction malloc_erreur est de O(1).

* matrice_creuse* init(int nMatrice)

```c
matrice_creuse* init(int nMatrice){
    matrice_creuse *m = malloc(nMatrice * sizeof(matrice_creuse)); //permet de donner de l’espace mémoire à la matrice
    malloc_erreur(m); // vérifier si on a bien obtenu une allocation
    printf("\n"
        "\t ____________________________________________________________\n"
        "\t|1. Remplir une matrice creuse                               |\n"
        "\t|2. Afficher une matrice creuse sous forme de tableau        |\n"
        "\t|3. Afficher une matrice creuse sous forme de listes         |\n"
        "\t|4. Donner la valeur d’un element d’une matrice creuse       |\n"
        "\t|5. Affecter une valeur a un element d’une matrice creuse    |\n"
        "\t|6. Additionner deux matrices creuses                        |\n"
        "\t|7. Calculer le gain en espace                               |\n"
        "\t|8. Quitter                                                  |\n"
        "\t|____________________________________________________________|\n"
        "*** Attention : les numeros (de matrice, de ligne, de colonne) commencent par 1 au lieu de 0 ***\n\n");
    return m;
}
```

Cela permet d'afficher les commandes possibles, de donner de l'espace mémoire à la matrice et de vérifier que l'on a obtenu une allocation mémoire.
La complexité de la première instruction est de O(1).
La complexité de la seconde intruction est O(1) car la fonction malloc_erreur est de O(1).
Puis le reste est aussi de O(1). Il n'y a aucun boucle dans la fonction init et la fonction malloc_erreur.
Donc la complexité de init est de O(1).

* int choixManip()

```c
int choixManip(){
    int choix;
    do{
        printf("\nQuelle est votre manipulation (entre 1 et 8) ? ");
        scanf("%d", &choix);
    }while(choix < 1 || choix > 8);
    return choix;
}
```

ChoixManip permet de demander à l'utilisateur de choisir entre les commandes précédemment présentées.
La boucle do while recommence pour choix entre – l’infini et 1 ou 8 et + l’infini. Donc si choix n’appartient pas à [1 ;8] alors il tend vers n.
Donc la complexité de la fonction choixManip est de O(n), n étant le nombre d'entrées invalides.

* int valElement()

```c
int valElement(){
    printf("Quelle est la valeur de l'element ? ");
    int val;
    scanf("%d", &val); // *** ne pas utiliser getPositif !
    return val;
}
```

valElement permet à l'utilisateur de de saisir la valeur de l'element.
La complexité de la fonction valElement est de O(1) car ici on a O(4)=O(1).

* int numeroMatrice(int max)

```c
int numeroMatrice(int max){
    printf("Quelle est le numero de la matrice a manipuler ? ");
    int n;
    do{
        printf("L'entree doit ne pas depasser le nombre total des matrices, %d. ", max);
        n = getPositif();
    }while(n > max);
    return n;
}
```

numeroMatrice permet à l'utilisateur de saisir le numero de la matrice à utiliser et de vérifier qu'elle correspond bien à une des matrices dont on a aloué de l'espace mémoire (en comparant tout simplement si on ne dépasse pas le nombre maximal de matrice saisie).
La première instruction est de O(1), pareil pour la deuxième.
Pour la boucle do while, le pire des cas est que l’on écrive n fois un n supérieur à max. Or ici, le n est rentré une fois (il n'y a pas de boucle présente pour le rentrer plusieurs fois). Cela donne une complexité de O(1). Dans le do, on fait appel à la fonction getPositif qui est de complexité O(m).
Donc la complexité de numeroMatrice est de O(m)

### Fonctions principales

Les codes sont à consulter dans les fichiers de code source.

* void remplirMatrice(matrice_creuse \*m, int N, int M);

On utilise un pointeur de variable pour modifier le contenu d'un variable
Parail, on utilise un pointeur de pointeur lorsque l'on veut modifier le contenu ce dernier pointeur, par exemple inserer un nouveau noeud
1ère instruction : O(1)
2ème instruction : O(1)
Etc O(1) jusqu’à la 6ème instruction (entre m->tableau... et int val).
Boucle for : on va de 0 à N. N étant le nombre de lignes au total.
Deuxième boucle for : on va de 0 à M, M étant le nombre de colonnes au total.
Le pire des cas possible est d'avoir des valeurs non nulles pour chaque colonne, et devoir donc créer un noeud pour chaque colonne.
Ainsi, la complexité de la fonction remplirMatrice est de O(N\*M).

* void afficherMatrice(matrice_creuse m);

La boucle for va de 0 à m.Nlignes, c’est-à-dire le numéro de lignes de la matrice m.
La deuxième boucle for va de 0 à m.Ncolonnes, c’est-à-dire le numéro de colonnes de la matrice m.
Ici, le pire des cas pour les boucles if est de continuer de remplir les 0. Or, pour remplir le maximum de zéro, il faut remplir la taille totale maximale de la matrice m. De plus, si il n'y a pas de zéro mais des chiffres à saisir, cela revient au même car le pire des cas est de remplir la totalité de chaque colonne de chaque ligne. Sa taille maximale est de m.Nlignes\*m.Ncolonnes : soit i\*j
Donc la complexité sera O(N\*M) avec N le nombre de lignes totale de la matrice m et M le nombre total de colonnes de la matrice m.

* void afficherMatriceListes(matrice_creuse m);

La boucle for va de 0 à m.Nlignes, c’est-à-dire le numéro de lignes de la matrice m.
Ici, le pire des cas est d’aller au bout (d’afficher tous les chiffres qu’il est possible d’avoir saisi) de la liste chainée car il n’y a pas de 0 (on ne saisit pas de zéro dans la liste chainée). Or, le bout de la liste chainée est au maximum de taille Ncolonne (nombre total de colonne de la matrice).
Donc la complexité sera O(N\*M) avec N le nombre de lignes totale de la matrice m et M le nombre total de colonnes de la matrice m.

* int rechercherValeur(matrice_creuse m, int i, int j);

Première partie avec les while :
Pour i : complexité de O(n)
Pour j : complexité de O(m)
N et m étant le nombre d’entrée utilisateur fausses.
Deuxième partie :
La complexité de la boucle while pour le i est de O(1). En effet, le i est la ligne d’un tableau des listes chainées. Donc, comme dans la fonction rechercherValeur on donne directement le i a trouver et qu’il se trouve dans un tableau, on a O(1).
On utilise le i transmet pour obtenir directement la tête de la ligne i, donc on a O(1).
Pour le j, c’est différent car c’est la colonne d’une liste chainée. Ainsi, on va être obligé de la parcourir pour arriver au j. Le pire des cas possible étant de ne pas trouver j après avoir tout parcouru : donc la complexité est de O(n), n étant ici la taille de la liste chainée parcourue.

* void affecterValeur(matrice_creuse m, int i, int j, int val);

La boucle if tourne lorsque val est égale à val0. Val0 est égale à rechercherValeur(m, i, j). Ainsi la boucle fonctionne si les deux valeurs sont identiques.
La seconde boucle if compare la valeur val à 0. Dans cette boucle, il existe une boucle tant que allant jusqu'à j.
La seconde boucle if compare val0 à 0. Et la boucle tant que ne tourne pas lorsque c'est supérieur à j ou égale à zéro. Ainsi, le pire des cas est que (\*ptr) ne soit pas dans ces cas là.
La dernière boucle tant que permet de changer la valeur du pointeur ptr et de désigner ainsi la val suivante.
Le pire des cas serait que toutes les boucles tournent: donc soit le premier if, soit les deux autres if. Mais la complexité sera de O(1) au final.

* void additionnerMatrices(matrice_creuse m1, matrice_creuse m2);

La boucle for va de 0 à m1.Nlignes, c'est-à-dire le numéro de la ligne de la matrice m1.
On observe ici des boucles permettant d'additionner les matrices: le pire des cas est de devoir additionner tous les elements composants la matrice. N est le nombre de lignes et M est le nombre de colonnes. Les deux matrices ayant la même taille, leur N et leur M sont similaires.
Cela donne une complexité de O(N\*M) avec le N et le M dépendant des conditions ci-dessus.

* int nombreOctetsGagnes(matrice_creuse m);

La deuxième itération est d'une complexité de O(1). Puis il y a la boucle for allant de 0 à m.Nlignes, c’est-à-dire le numéro de ligne de la matrice m. A l'intérieur de la boucle, la première itération est de O(1). Ensuite, la boucle tant que permet de compter le nombre des noeuds. Ainsi, le pire cas est que le nombre des noeuds soit maximal: c'est-à-dire que cela atteigne la taille maximale de la matrice en "largeur" (M). Donc le while peut aller dans le pire des cas jusqu'à M et le for jusqu'à N. Ainsi, cela signifie une complexité de O(N\*M).
Par la suite, les itérations suivantes sont de O(1). Ce qui l'emporte donc est de O(N\*M).
