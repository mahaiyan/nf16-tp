#include <stdio.h>
#include <stdlib.h>

struct Passage_A
{
    int id;
    bool type_p;
    char[16] date_heure;
    struct Passage_A*fils_gauche;
    struct Passage_A*fils_droit;
};

typedef struct Passage_A PassageABR;
typedef struct PassageABR* arbre;

void insererABR_p(Arbre*arbre, PassageABR);
void afficherABR(Arbre*arbre);
void supprimerABR_p(Arbre*arbre, int identifiant);
void hauteurABR(Arbre*arbre);
void detruireABR(Arbre*arbre);
