#ifndef TP3_H_INCLUDED
#define TP3_H_INCLUDED // comment ça fonctionne ?

typedef struct element{
    int col;
    int val;
    struct element* suivant;
}element;

typedef element* liste_ligne;

typedef struct matrice_creuse{
    liste_ligne* tableau; // pointeur de pointeur
    int Nlignes;
    int Ncolonnes;
}matrice_creuse;


void remplirMatrice(matrice_creuse *m, int N, int M);
void afficherMatrice(matrice_creuse m);
void afficherMatriceListes(matrice_creuse m);
int rechercherValeur(matrice_creuse m, int i, int j);
void affecterValeur(matrice_creuse m, int i, int j, int val);
void additionnerMatrices(matrice_creuse m1, matrice_creuse m2);
int nombreOctetsGagnes(matrice_creuse m);

#include "tp3.c"
#endif // TP3_H_INCLUDED
