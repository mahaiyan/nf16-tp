#include "tp3.h"
#define nl 5
#define nc 6
#define str1 "\
5 0 2 7 4 0 0 1 1 0 0 0 0 0 0 0 0 0 0 3 0 2 0 0 6 0 0 0 0 2\
"
#define str2 "\
1 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0\
"
void createList(matrice_creuse *m, int* tab);
void strtointtab(char *s, int *tab){
    int *pt = tab;
    int i=0;
    char *ps = s;
    while(*ps){
        *pt = atoi(ps);
        ++pt;
        while(*ps && *ps != ' ') ++ps;
        while(*ps == ' ') ++ps;
    }
}

int main(){
    matrice_creuse m1, m2;
    char *s1 = str1, *s2 = str2;
    int tab1[nl*nc], tab2[nl*nc];
    strtointtab(s1, tab1);
    strtointtab(s2, tab2);
    createList(&m1, tab1);
    createList(&m2, tab2);
    additionnerMatrices(m1, m2);
    afficherMatrice(m1);
    /* 
    int i,j;
    for(i=0; i<nl; ++i){
        for(j=0; j<nc; ++j) printf("%d\t", tab1[i*nl+j]);
        printf("\n");
    }
    printf("\n");
    
    for(i=0; i<nl; ++i){
        for(j=0; j<nc; ++j) printf("%d\t", tab2[i*nl+j]);
        printf("\n");
    } */
    return 0;
}

void createList(matrice_creuse *m, int* tab){
    m->tableau = malloc(nl * sizeof(liste_ligne));
    m->Nlignes = nl;
    m->Ncolonnes = nc;
    int val;
    element** ptr;
    element* tmp;
    int j;
    for(int i=0; i<nl; i++){
        ptr = m->tableau + i;
        *ptr = 0;
        for(j=0; j<nc; j++){
            val = *(tab + i*nc + j);
            if(!val) continue;
            tmp = malloc(sizeof(element));
            tmp->col = j;
            tmp->val = val;
            tmp->suivant = 0;
            *ptr = tmp;
            ptr = &(tmp->suivant);
        }
    }
}