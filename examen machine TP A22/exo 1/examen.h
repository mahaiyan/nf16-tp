#include <stdio.h>
#include <stdlib.h>

struct Passage_e
{
    int id;
    bool type_p;
    char date_heure;
    struct Passage_e* suivant;
};

typedef struct Passage_e passage;

typedef passage* liste;
void insererL_p(Liste* liste, int iden);
void afficherL(Liste liste);
void supprimerL_p(Liste* liste);
void detruireL(Liste* liste);
