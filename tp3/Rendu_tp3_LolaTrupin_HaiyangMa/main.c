#include <stdio.h>

#include "tp3.h"

int main(){
    int nMatrice = nombreMatrice();
    matrice_creuse* m = init(nMatrice);
    int numero, numero2;
    int i, j, val;

    int choix;
    do{
        choix = choixManip();
        switch (choix) // *** pourquoi pas utilisier les pointeurs pour transmettre les matrices ???
        {
        case 1:
            numero = numeroMatrice(nMatrice) - 1;
            int nLigne = nombreLigne();
            int nColonne = nombreColonne();
            remplirMatrice(m + numero, nLigne, nColonne);
            break;
        case 2:
            numero = numeroMatrice(nMatrice) - 1;
            afficherMatrice(*(m + numero));
            printf("\n");
            break;
        case 3:
            numero = numeroMatrice(nMatrice) - 1;
            afficherMatriceListes(*(m + numero));
            printf("\n");
            break;
        case 4:
            numero = numeroMatrice(nMatrice) - 1;
            i = ligneElement(); // *** il faut verifier la validite ***
            j = colonneElement();
            printf("La valeur recherchee est %d !\n", rechercherValeur(*(m+numero), i, j));
            break;
        case 5:
            numero = numeroMatrice(nMatrice) - 1;
            i = ligneElement();
            j = colonneElement();
            val = valElement();
            affecterValeur(*(m+numero), i, j, val);
            break;
        case 6:
            printf("Pour la premiere matrice :\n");
            numero = numeroMatrice(nMatrice) - 1;
            // printf("La premiere matrice est la matrice %d", numero);
            printf("Pour la deuxieme matrice :\n");
            numero2 = numeroMatrice(nMatrice) - 1;
            // printf("La deuxieme matrice est la matrice %d", numero2);
            additionnerMatrices(*(m + numero), *(m + numero2));

            afficherMatrice(*(m+numero));
            break;
        case 7: // ***
            numero = numeroMatrice(nMatrice) - 1;
            val =  nombreOctetsGagnes(*(m + numero));
            if(val > 0)
                printf("On a gagne %d octets !!!\n", val);
            else
                printf("On a perdu %d octets...\n", -val);
            break;
        default:
            break;
        }
    }while(choix != 8);
    printf("Merci pour votre utilisation ! ");
    // commencer a liberer la memoire
    for(int n=0; n<nMatrice; n++){
        supprimerMatrice(m+n); // *** faut ne pas afficher les phrases
    }
    free(m);
    m = 0;
    return 0;
}
