#ifndef tp4_h
#define tp4_h

#define CMAX 50
#define DMAX 11
#define SMAX 1024

#define getString(print, var) printf("%s", print); scanf("%s", var)
#define getInt(print, var) printf("%s", print); scanf("%d", &var)

#define Nom "Nom du patient : "
#define Prenom "Prenom du patient : "
#define Date "Date de consultation : "
#define Motif "Motif de consultation : "
#define Niveau "Niveau d'urgence : "

typedef struct Consultation_{
    char* date;
    char* motif;
    int niveauUrg;
    struct Consultation_* suivant;
}Consultation;

typedef struct Patient_
{
    char* nom;
    char* prenom;
    Consultation* ListeConsult;
    int nbrconsult;
    struct Patient_* fils_gauche;
    struct Patient_* fils_droit;
}Patient;

typedef Patient* Parbre;


void inserer_patient(Parbre* abr, char* nm, char* pr);
void ajouter_consultation(Parbre* abr, char* nm, char* date, char* motif,int nivu);
void afficher_fiche(Parbre* abr, char* nm);
void afficher_patient(Parbre* abr);
void supprimer_patient(Parbre* abr, char* nm);
void maj(Parbre* abr, Parbre* abr2);

int choix_fonction();


#include "tp4.c"
#endif
