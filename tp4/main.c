#include "tp4.h"
#include <stdio.h>


int main(){
    int choix = 0;
    Parbre abr = 0, abr2 = 0; //Initialisation !
    char nm[CMAX], pr[CMAX], date[DMAX], motif[SMAX];
    int nivu;

    while(choix != 7){
        choix = choix_fonction();
        switch (choix)
        {
        case 1:
            getString(Nom, nm);
            strupr_(nm);
            getString(Prenom, pr);
            inserer_patient(&abr, nm, pr);
            break;

        case 2:
            getString(Nom, nm);
            strupr_(nm);
            // Patient *p = rechercher_patient(&abr, nm);
            // if(!p){
            //     printf("Patient introuve.\n");
            //     break;
            // }

            getString(Date, date);
            getString(Motif, motif);
            getInt(Niveau, nivu);
            ajouter_consultation(&abr, nm, date, motif, nivu);
            break;

        case 3:
            getString(Nom, nm);
            strupr_(nm);
            afficher_fiche(&abr, nm);
            break;

        case 4:
            printf("Liste des patients :\n");
            afficher_patient(&abr);
            printf("Liste terminee.\n");
            printf("Copie de liste :\n");
            afficher_patient(&abr2);
            printf("Copie terminee.\n");
            break;

        case 5:
            getString(Nom, nm);
            strupr_(nm);
            supprimer_patient(&abr, nm);
            break;

        case 6:
            maj(&abr, &abr2);
            printf("Liste mis a jour.\n");
            break;

        case 7:
            supprimerArbre(&abr);
            supprimerArbre(&abr2);
            printf("Arbres supprimes.\n");
            break;

        default:
            break;
        }
    }
}
