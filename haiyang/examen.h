// examen.h
#ifndef examen_h
#define examen_h

typedef struct Passage_e
{
    int id;
    char* type_p;
    char* date_heure;
    struct Passage_e* suivant;
}Passage;

typedef Passage* liste;

char* entree = "entree";
char* sortie = "sortie";

void insererL_p (liste* l, int id, char type, char* date);
void afficherL(liste l);
void supprimerL_p (liste* l, int id);
void detruireL(liste *l);

#include "examen.c"

#endif