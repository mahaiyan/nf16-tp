// test.c
#include "examen.h"

int main(){
    int id[] = {1,1,2,3,4};
    char type[] = {"esese"};
    char* date[] = {"JJ-MM-AAAA_HH:MM","04-01-2023_15:38", "13-12-2022_09:56", "09-10-2020_03:43", "01-23-2020_06:00"};
    liste l = 0;
    for(int i = 0; i < 5; ++i){
        insererL_p(&l, id[i], type[i], date[i]);
        afficherL(l);
        printf("\n");
    }
    supprimerL_p(&l, 1);
    afficherL(l);
    detruireL(&l);
    return 0;
}