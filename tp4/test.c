#include "tp4.h"
#include <stdio.h>
#include <string.h>


void testStrupr(){
    char s[] = "lower case";
    strupr_(s);
    printf("%s",s);
}

void testMaj(){
    Parbre abr = 0, abr2 = 0;
}

void testConsultation(){
    Parbre abr = 0, abr2 = 0;
    char *nom[] = {"MA", "Z", "A"}, *prenom[] = {"haiyang", "zz", "aa"};
    int i, n = 3;
    for(i = 0; i < n; ++i){
        printf("%s %s\n", nom[i], prenom[i]);
        inserer_patient(&abr,nom[i], prenom[i]);
    }
    afficher_patient(&abr);
    supprimer_patient(&abr, nom[1]);
    ajouter_consultation(&abr, nom[1], "11-12-2022", "cold", 5);
    ajouter_consultation(&abr, nom[0], "11-12-2022", "cold", 5);
    ajouter_consultation(&abr, nom[0], "01-01-1999", "hot", 4);
    afficher_fiche(&abr, nom[0]);

    supprimerArbre(&abr);
    printf("Arbres supprimes.\n");
}

void testSup(){
    Parbre abr = 0, abr2 = 0;
    char *nom[] = {"T", "L", "A", "C", "D", "M"}, *prenom[] = {"tt", "ll", "aa", "cc", "dd", "mm"};
    int i, n = 6;
    for(i = 0; i < n; ++i){
        printf("%s %s\n", nom[i], prenom[i]);
        inserer_patient(&abr,nom[i], prenom[i]);
    }
    int a = 1, b = 4; // L, D
    ajouter_consultation(&abr, nom[a], "11-12-2022", "cold", 5);
    ajouter_consultation(&abr, nom[b], "01-01-1999", "hot", 4);
    afficher_patient(&abr);
    afficher_fiche(&abr, nom[a]);
    afficher_fiche(&abr, nom[b]);
    supprimer_patient(&abr, nom[a]);
    afficher_patient(&abr);

    supprimerArbre(&abr);
    printf("Arbres supprimes.\n");
}

int main(){
    testSup();
    // testConsultation();
}