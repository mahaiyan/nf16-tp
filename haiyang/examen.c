#include "examen.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// #include <stdbool.h>

char* getType();
Passage** findP(liste* l, int id);

void insererL_p (liste* l, int id, char type, char* date){ //'e' pour entrée, 's' pour sortie
    Passage* p= (Passage*)malloc(sizeof(Passage));
    p->id = id;
    if(type == 'e') p->type_p = entree;
    else if(type == 's') p->type_p = sortie;
    else p->type_p = getType();
    p->date_heure = strdup(date); // strdup == malloc + strcpy, donc free plus tard

    Passage** find = findP(l, id);
    while(*find && (*find)->id == id) find = &(*find)->suivant; // le dernier passage
    p->suivant = *find;
    *find = p;
}

char* getType(){
    char c;
    do{
    printf("Veuiller choisir le type du passage : 'e'/'s'");
    scanf("%c", &c);
    }while(c!='e' && c!='s');
    if(c == 'e') return entree;
    else return sortie;
}

Passage** findP(liste* l, int id){
    Passage** p = l;
    while (*p && (*p)->id != id){ // le premier passage
        p = &(*p)->suivant;
    }
    return p;
}

void afficherL(liste l){
    while(l){
        printf("id : %d\ttype : %s\tdate_heure : %s\n", l->id, l->type_p, l->date_heure);
        l = l->suivant;
    }
}

void supprimerL_p (liste* l, int id){
    Passage** find = findP(l, id);
    Passage* tmp = *find;
    *find = (*find)->suivant;
    free(tmp->date_heure);
    free(tmp);
}

void detruireL(liste *l){
    Passage* tmp;
    while(*l){
        tmp = *l;
        *l = (*l)->suivant;
        free(tmp->date_heure);
        free(tmp);
    }
}