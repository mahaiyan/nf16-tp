#include "tp4.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define scpy(dest, src) dest = malloc(sizeof(src)); strcpy(dest, src)

#define Choix "Choix non valide. Veuillez rentrer votre choix : "

void strupr_(char* s);
Patient* CreerPatient(char* nm, char* pr);
Patient * rechercher_patient(Parbre * abr, char* nm);
Patient** emplacementPatient(Parbre* abr, char* nm); // différente que l'énoncé
Consultation* CreerConsult(char* date, char* motif, int nivu);
void supprimerListe(Consultation *c);
void freePatient(Patient *p);
void majIns(Parbre *abr, Parbre *abr2);
void majSup(Parbre *abr, Parbre *abr2);
void supprimerArbre(Parbre* abr);


void strupr_(char* s){
    while(*s != '\0'){
        if(*s >= 'a' && *s <= 'z') *s += 'A' - 'a';
        ++s;
    }
}

Patient* CreerPatient(char* nm, char* pr){
    Patient* p = malloc(sizeof(Patient));
    scpy(p->nom, nm);
    scpy(p->prenom, pr);
    p->ListeConsult = 0;
    p->nbrconsult = 0;
    p->fils_gauche = 0;
    p->fils_droit = 0;
    return p;
}

Patient * rechercher_patient(Parbre * abr, char* nm){
    Patient *p = *abr;
    int cmp;
    while(p){
        cmp = strcmp(p->nom, nm);
        if(cmp > 0) p = p->fils_gauche;
        else if(cmp < 0) p = p->fils_droit;
        else break;
    }
    return p;
}

void inserer_patient(Parbre* abr, char* nm, char* pr){ //double pointeur
    Patient **p = emplacementPatient(abr, nm);
    if(*p){
        printf("Patient existe.\n");
        return;
    }
    *p = CreerPatient(nm, pr);
    printf("Patient ajoute.\n");
}

Patient **emplacementPatient(Parbre *abr, char *nm){
    Patient **p = abr;
    int cmp;
    while(*p){
        cmp = strcmp((*p)->nom, nm);
        if(cmp > 0) p = &(*p)->fils_gauche;
        else if(cmp < 0) p = &(*p)->fils_droit;
        else break;
    }
    return p;
}

Consultation* CreerConsult(char* date, char* motif, int nivu){
    Consultation *c = malloc(sizeof(Consultation));
    scpy(c->date, date);
    scpy(c->motif, motif);
    c->niveauUrg = nivu;
    return c;
}

void ajouter_consultation(Parbre* abr, char* nm, char* date, char* motif, int nivu){
    Patient **p = emplacementPatient(abr, nm);
    if(!*p){
        printf("Patient introuve.\n");
        return;
    }
    // printf("patient searched\n");
    Consultation **c = &(*p)->ListeConsult;
    while(*c){
        c = &(*c)->suivant;
    }
    // printf("consultation traversed\n");
    *c = CreerConsult(date, motif, nivu);
    ++(*p)->nbrconsult;
    printf("Consultation ajoutee.\n");
}

void afficher_fiche(Parbre* abr, char* nm){
    Patient *p = rechercher_patient(abr, nm);
    if(!p){
        printf("Patient introuve.\n");
        return;
    }
    printf("\nFiche du patient :\n");
    printf("Nom : %s\tPrenom : %s\tNombre de consultations : %d\n", (p)->nom, (p)->prenom, (p)->nbrconsult);
    int i = 0;
    Consultation *c = (p)->ListeConsult;
    for(; i < (p)->nbrconsult; ++i){
        printf("Consultation %d\n\tDate : %s\tNiveau d'urgence : %d\tMotif : %s\n\n", i+1, c->date, c->niveauUrg, c->motif);
        c = c->suivant;
    }
}

void afficher_patient(Parbre* abr){ //pourquoi pas Inorder Traversal ?
    if(!*abr) return;
    printf("Nom : %s\tPrenom : %s\t\n", (*abr)->nom, (*abr)->prenom);
    afficher_patient(&(*abr)->fils_gauche);
    afficher_patient(&(*abr)->fils_droit);
}

void supprimerListe(Consultation *c){
    Consultation *tmp = c;
    while(tmp){
        c = c->suivant;
        free(tmp);
        tmp = c;
    }
}

void freePatient(Patient *p){
    free(p->nom);
    free(p->prenom);
    supprimerListe(p->ListeConsult);
    free(p);
}

void supprimer_patient(Parbre* abr, char* nm){
    Patient **p = emplacementPatient(abr, nm); //p points to [the case pointing the node to delete]
    Patient *tmp = *p; //tmp points to [the node to delete]
    if(!*p){
        printf("Patient introuve.\n");
        return;
    }
    else if(!(*p)->fils_gauche && !(*p)->fils_droit){ //00
        // printf("00");
        *p = 0;
    }
    else if(!(*p)->fils_gauche){ //01
        // printf("01");
        *p = (*p)->fils_droit;
    }
    else if(!(*p)->fils_droit){ //10
        // printf("10");
        *p = (*p)->fils_gauche;
    }
    // 11
    else if(!tmp->fils_gauche->fils_droit){
        tmp->fils_gauche->fils_droit = tmp->fils_droit;
        *p = tmp->fils_gauche;
    }
    else{
        Patient **p2 = &tmp->fils_gauche; //p2 points to [the case pointing the node to replace]
        while((*p2)->fils_droit) p2 = &(*p2)->fils_droit;
        // printf("p2 : %s\n", (*p2)->nom);
        *p = *p2;
        *p2 = (*p2)->fils_gauche;
        // printf("left of p2\n");
        (*p)->fils_gauche = tmp->fils_gauche;
        // printf("left of tmp : %s\n", tmp->fils_gauche->nom);
        (*p)->fils_droit = tmp->fils_droit;
        // printf("copier tmp -> p2\n");
    }
    freePatient(tmp);
    printf("Patient supprime.\n");
    return;
}

void majIns(Parbre *abr, Parbre *abr2){ //Pre-Order Traversal, sinon l'arbre deviendra "une liste chaîné"
    if(!*abr) return;
    inserer_patient(abr2, (*abr)->nom, (*abr)->prenom);
    majIns(&(*abr)->fils_gauche, abr2);
    majIns(&(*abr)->fils_droit, abr2);
}

void majSup(Parbre *abr, Parbre *abr2){ //Post-Order Traversal, pour traverser des feuilles vers racine
    if(!*abr2) return;
    majSup(abr, &(*abr2)->fils_gauche);
    majSup(abr, &(*abr2)->fils_droit);
    if(!*emplacementPatient(abr, (*abr2)->nom)) supprimer_patient(abr2, (*abr2)->nom);
}

void maj(Parbre* abr, Parbre* abr2){
    majSup(abr, abr2);
    majIns(abr, abr2);
}

void supprimerArbre(Parbre* abr){
    if(*abr){
        supprimerArbre(&(*abr)->fils_gauche);
        supprimerArbre(&(*abr)->fils_droit);
        freePatient(*abr);
        (*abr) = 0;
    }
}

int choix_fonction(){
    printf("\n"
        "\t ------------------------------------------------- \n"
        "\t|1. Ajouter un patient                            |\n"
        "\t|2. Ajouter une consultation a un patient         |\n"
        "\t|3. Afficher une fiche medicale                   |\n"
        "\t|4. Afficher la liste des patients                |\n"
        "\t|5. Supprimer un patient                          |\n"
        "\t|6. Copier ou mettre a jour la liste des patients |\n"
        "\t|7. Quitter                                       |\n"
        "\t ------------------------------------------------- \n"
        "Votre choix (entre 1 et 7) : ");
    int choix;
    scanf("%d", &choix);
    while(choix < 1 || choix > 7){ //Crochet est nécéssaire pour le macro !
        getInt(Choix, choix);
    }
    return choix;
}
