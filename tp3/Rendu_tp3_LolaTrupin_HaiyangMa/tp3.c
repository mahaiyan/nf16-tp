#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "tp3.h"

#define print0 printf("0\t")
#define printfin printf("fin\n")
#define printval(X) printf("%d\t", X)
#define printinfo(COL,VAL) printf("col:%d val:%d\t",COL,VAL)
// utiliser macro pour une structure qui a plus de sens et qui se modifie plus facilement

extern int errno;

void supprimerMatrice(matrice_creuse *m){
    int i, j;
    element** ptr;
    element* tmp;
    for(i=0; i<m->Nlignes; i++){
        ptr = m->tableau + i;
        while(*ptr){
            tmp = *ptr;
            ptr = &(tmp->suivant);
            free(tmp);
        }
    }
    free(m->tableau);
    printf("Matrice supprimee.\n");
}

void malloc_erreur(void* p){
    if(!p)
        perror("Espace insuffisant. ");
}

int getPositif(){
    int n;
    do{
        printf("L'entree doit être strictement positif : ");
        scanf("%d", &n);
    }while(n < 1);
    return n;
}

int nombreMatrice(){
    printf("Quel est le nombre total des matrices ? ");
    return getPositif();
}

matrice_creuse* init(int nMatrice){
    matrice_creuse *m = malloc(nMatrice * sizeof(matrice_creuse));
    malloc_erreur(m); // vérifier si on a bien obtenu une allocation
    printf("\n"
        "\t ____________________________________________________________\n"
        "\t|1. Remplir une matrice creuse                               |\n"
        "\t|2. Afficher une matrice creuse sous forme de tableau        |\n"
        "\t|3. Afficher une matrice creuse sous forme de listes         |\n"
        "\t|4. Donner la valeur d’un element d’une matrice creuse       |\n"
        "\t|5. Affecter une valeur a un element d’une matrice creuse    |\n"
        "\t|6. Additionner deux matrices creuses                        |\n"
        "\t|7. Calculer le gain en espace                               |\n"
        "\t|8. Quitter                                                  |\n"
        "\t|____________________________________________________________|\n"
        "*** Attention : les numeros (de matrice, de ligne, de colonne) commencent par 1 au lieu de 0 ***\n\n");
    return m;
}

int choixManip(){
    int choix;
    do{
        printf("\nQuelle est votre manipulation (entre 1 et 8) ? ");
        scanf("%d", &choix);
    }while(choix < 1 || choix > 8);
    return choix;
}

int nombreLigne(){
    printf("Quelle est le nombre de ligne de la matrice ? ");
    return getPositif();
}

int nombreColonne(){
    printf("Quelle est le nombre de colonne de la matrice ? ");
    return getPositif();
}

int ligneElement(){
    printf("Quelle est le numero de ligne de l'element ? ");
    return getPositif();
}

int colonneElement(){
    printf("Quelle est le numero de colonne de l'element ? ");
    return getPositif();
}

int valElement(){
    printf("Quelle est la valeur de l'element ? ");
    int val;
    scanf("%d", &val); // *** ne pas utiliser getPositif !
    return val;
}

int numeroMatrice(int max){
    printf("Quelle est le numero de la matrice a manipuler ? ");
    int n;
    do{
        printf("L'entree doit ne pas depasser le nombre total des matrices, %d. ", max);
        n = getPositif();
    }while(n > max);
    return n;
}

void remplirMatrice(matrice_creuse *m, int N, int M){
    m->tableau = malloc(N * sizeof(liste_ligne));
    malloc_erreur(m->tableau); // vérifier si on a bien obtenu une allocation
    m->Nlignes = N;
    m->Ncolonnes = M;
    printf("Veuillez entrer une matrice de la taille %d * %d :\n", N, M);
    int val;
    for(int i=0; i<N; i++){
        element** ptr = m->tableau + i;
        *ptr = 0;
        for(int j=0; j<M; j++){
            scanf("%d", &val);
            if(!val)
                continue;
            // val != 0
            element* tmp = malloc(sizeof(element));
            malloc_erreur(tmp); // vérifier si on a bien obtenu une allocation
            tmp->col = j;
            tmp->val = val;
            tmp->suivant = 0;
            *ptr = tmp;
            ptr = &(tmp->suivant);
        }
    }
}

void afficherMatrice(matrice_creuse m){
    int i,j;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        for(j=0; j<m.Ncolonnes; j++){
            if(!ptr){ // traiter la fin de la chaîne
                for(; j<m.Ncolonnes; j++)
                    print0;
                break;
            }
            if(ptr->col > j){ // remplir les 0
                for(; j<ptr->col; j++)
                    print0;
            }
            // ptr->col == j
            printval(ptr->val);
            ptr = ptr->suivant;
        }
        printf("\n");
    }
}

void afficherMatriceListes(matrice_creuse m){
    int i;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        while(ptr){
            printinfo(ptr->col, ptr->val);
            ptr = ptr->suivant;
        }
        // la fin de la chaîne
        printfin;
    }
}

int rechercherValeur(matrice_creuse m, int i, int j){
    while(i < 1 || i > m.Nlignes){
        printf("Numero de la ligne incorrect, entrez de nouveau : ");
        scanf("%d", &i);
    }
    while(j < 1 || j > m.Ncolonnes){
        printf("Numero de la colonne incorrect, entrez de nouveau : ");
        scanf("%d", &j);
    }
    i--;
    j--;
    element* ptr = *(m.tableau + i);
    while(ptr && ptr->col < j){
        ptr = ptr->suivant;
    }
    if(!ptr) // chaîne terminée avant la colonne j
        return 0;
    if(ptr->col == j) // noeud trouvé
        return ptr->val;
    // ptr->col > j
    // le noeud n'existe pas, le valeur est donc 0
    return 0;
}

void affecterValeur(matrice_creuse m, int i, int j, int val){
    // verifier la validite de i et j dans la fonction rechercherValeur
    int val0 = rechercherValeur(m, i, j); // matrice creuse donc val0 == 0 la plus part du temp
    // l'ordre est spécifié pour qu'il soit moins coûteux que possible
    i--;
    j--;
    if(val == val0){ // rien à modifier
        printf("valeur identique.\n");
        return;
    }
    if(val == 0){ // val0 != 0, supprimer un noeud
        element** ptr = m.tableau + i;
        element* tmp;
        while((*ptr)->col < j) // val0 non 0 donc noeud existe toujours
            ptr = &(*ptr)->suivant;
        // (*ptr)->col == j
        tmp = *ptr;
        *ptr = tmp->suivant;
        free(tmp);
        printf("noeud supprime.\n");
        return;
    }
    if(val0 == 0){ // val != 0, rajouter un noeud
        element** ptr = m.tableau + i;
        while ((*ptr) && (*ptr)->col < j) // chercher
            ptr = &((*ptr)->suivant);
        // (*ptr)->col > j ou (*ptr)==0, donc trouvé
        element *tmp = malloc(sizeof(element)); // *** déboggué ***
        tmp->col = j;
        tmp->val = val;
        tmp->suivant = *ptr;
        *ptr = tmp;
        printf("noeud rajoute.\n");
        return;
    }
    // noeud existe déjà, changer la valeur
    element* ptr = *(m.tableau + i);
    while(ptr->col < j){
        ptr = ptr->suivant;
    }
    ptr->val = val;
    printf("valeur changee.\n");
    return;
}

void additionnerMatrices(matrice_creuse m1, matrice_creuse m2){
    element **p1, **p2, *t;
    if(m1.Nlignes != m2.Nlignes || m1.Ncolonnes != m2.Ncolonnes){
        printf("Les dimensions des deux matrices sont differentes, il n'est pas possible de les additionner. Entrez des matrices de taille similaire.\n");
        return;
    }

    for(int i=0; i<m1.Nlignes; i++){
        p1 = m1.tableau + i;
        p2 = m2.tableau + i;
        while (*p2) // on traite m2, pour modifier m1
        {
            while (*p1 && (*p1)->col < (*p2)->col)
                p1 = &((*p1)->suivant);
            if(!*p1) break;
            if((*p1)->col > (*p2)->col){ // noeud repéré, copier un seul noeud
                t = malloc(sizeof(element));
                t->col = (*p2)->col;
                t->val = (*p2)->val;
                t->suivant = *p1;
                *p1 = t;
                p1 = &(t->suivant);
            }
            else{ // (*p1)->col == (*p2)->col, ajouter les valeurs
                (*p1)->val += (*p2)->val;
                if(!(*p1)->val){
                    t = (*p1)->suivant;
                    free(*p1);
                    *p1 = t;
                }
            }
            p2 = &((*p2)->suivant); // noeud traité, avancer
        }
        if(!*p1){ // m1 terminé, copier tous les noeud de m2
            while(*p2){
                t = malloc(sizeof(element)); // *** utlisier toujours malloc pour ajouter un noeud ***
                t->col = (*p2)->col;
                t->val = (*p2)->val;
                t->suivant = 0;
                *p1 = t;
                p1 = &(t->suivant);
                p2 = &((*p2)->suivant); // noeud traité, avancer
            }
            // break; // terminer la ligne
        }
    }
}

int nombreOctetsGagnes(matrice_creuse m){
    int i,s=0;
    for(i=0; i<m.Nlignes; i++){
        element* ptr = *(m.tableau + i);
        while(ptr){
            ptr = ptr->suivant;
            s++; // compter le nombre des noeuds
        }
    }
    s *= sizeof(element); // octets des noeuds, incluant l'information de colonne et pointeur
    s += m.Nlignes * sizeof(liste_ligne); // octets de liste_ligne
    s += sizeof(matrice_creuse); // octets de matrice
    return m.Ncolonnes * m.Nlignes * sizeof(int) - s;
}
